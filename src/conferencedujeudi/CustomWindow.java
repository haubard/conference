package conferencedujeudi;

import Panels.AddConferencePanel;
import Panels.Home;
import Panels.VoirListeConference;
import java.awt.Color;
import java.awt.Event;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.JFrame;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class CustomWindow extends JFrame implements ActionListener {

    JMenuItem sousmenu1 = new JMenuItem("Fermer");
    JMenuItem sousmenu2 = new JMenuItem("Voir la liste des conférences");
    JMenuItem sousmenu3 = new JMenuItem("Ajouter une conférence");
    JMenuItem sousmenu4 = new JMenuItem("Modifier une conférence");
    JMenuItem sousmenu5 = new JMenuItem("Supprimer une conférence");
    JMenuItem sousmenu6 = new JMenuItem("M'inscrire à une conférence");

    // Mise en fonction de l'action "Fermer" sur le bouton de menu //////
    @Override
    public boolean action(Event evt, Object what) {
        return super.action(evt, what);
    }
// Sur le clic du menu fermer, fermeture de la fenetre

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == sousmenu1) {
            System.exit(0);
        }
        if (e.getSource() == sousmenu3) {
            AddConferencePanel ajouterConf = new AddConferencePanel();
            this.setContentPane(ajouterConf);
            this.repaint();
            this.revalidate();
        }
    }

    public CustomWindow() {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu1 = new JMenu("Fichier");
        JMenu menu2 = new JMenu("Conférence");
        Home panel = new Home();
        sousmenu1.addActionListener((ActionListener) this);
        sousmenu3.addActionListener(this);

        // rendre la fen visible/////
        this.setVisible(true);
        // dimension de la fen ///////
        this.setSize(1800, 1500);
        // Nom de la fen  //////////
        this.setTitle("APPLICATION_AD: Les conférences du Jeudi");
        // Centrer la fen dans l'écran /////
        this.setLocationRelativeTo(null);
        // Interdire la rtedimension ///////////////////
        this.setResizable(false);
        // permettre la fermeture de la fen avec la 'X' /////
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Affectation du menu à la fen ///////////////
        this.setJMenuBar(menuBar);
        // Affichage de Fichier ///////////////
        menuBar.add(menu1);
        // Affichage de Conférence ///////////
        menuBar.add(menu2);
        // Affichage des sous menu  ///////
        menu1.add(sousmenu1);
        menu2.add(sousmenu2);
        menu2.add(sousmenu3);
        menu2.add(sousmenu4);
        menu2.add(sousmenu5);
        menu2.add(sousmenu6);
        sousmenu1.setBackground(Color.LIGHT_GRAY);
        sousmenu2.setBackground(Color.LIGHT_GRAY);
        sousmenu3.setBackground(Color.LIGHT_GRAY);
        sousmenu4.setBackground(Color.LIGHT_GRAY);
        sousmenu5.setBackground(Color.LIGHT_GRAY);
        sousmenu6.setBackground(Color.LIGHT_GRAY);

        this.setContentPane(panel);
        this.repaint();
        this.revalidate();

        URL adImg = CustomWindow.class.getResource("/Image/icon.jpg");
        Image icon = Toolkit.getDefaultToolkit().getImage(adImg);
        this.setIconImage(icon);

    }

}
