package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Theme;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThemeDAO {

    /////////////////// methode getAll ////////////////////////////////////
    public static ArrayList<Theme> getAll() {

        ArrayList<Theme> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM theme";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idTheme = resR.getInt("idTheme");
                    String designationTheme = resR.getString("designationTheme");
                    Theme the = new Theme(idTheme, designationTheme);
                    listeRetour.add(the);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println();
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Theme get(int idTheme) {

        PreparedStatement ps = null;
        ResultSet resR = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT designationTheme FROM theme WHERE idTheme = ?";
        Theme the = null;
        ResultSet res = null;
        try {

            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idTheme);
            if (ps.execute()) {
                resR = ps.getResultSet();
            }

            try {
                while (resR.next()) {
                    String designationTheme = resR.getString("designationTheme");
                    the = new Theme(idTheme, designationTheme);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return the;

    }

////////////////////////////////////////////// creation de la methode INSERT ////////////////////////////////
    public static void insert(Theme the) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO theme"
                + "(designationTheme)"
                + "VALUES(?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, the.getDesignationTheme());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

//////////////////////////////////////// creation de la methode DELETE /////////////////////////////////////////////
    public void delete(int idTheme) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO theme WHERE idTheme =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idTheme);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

///////////////////////// creation de la methode UPDATE ////////////////////////////////////////
    public void update(Theme the) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO theme"
                + "(idTheme, designationTheme )"
                + "VALUES(?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, the.getIdTheme());
            ps.setString(2, the.getDesignationTheme());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
