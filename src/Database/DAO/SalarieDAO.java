package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Salarie;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalarieDAO {

// creation de la methode getAll //////////////////////////////////////////////////////
    public static ArrayList<Salarie> getAll() {

        ArrayList<Salarie> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM salarie";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idSalarie = resR.getInt("idSalarie");
                    String nomPrenomSalarie = resR.getString("nomPrenomSalarie");
                    Salarie sal = new Salarie(idSalarie, nomPrenomSalarie);
                    listeRetour.add(sal);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println();
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Salarie get(int idSalarie) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT nomPrenomSalarie FROM salarie WHERE idSalarie = ?";
        Salarie sal = null;
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idSalarie);
            resR = ps.executeQuery();

            try {
                while (resR.next()) {
                    String nomPrenomSalarie = resR.getString("nomPrenomSalarie");

                    sal = new Salarie(idSalarie, nomPrenomSalarie);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return sal;

    }

    // creation de la methode INSERT 
    public static void insert(Salarie sal) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO salarie"
                + "(nomPrenomSalarie)"
                + "VALUES(?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, sal.getNomPrenomSalarie());

            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode DELETE 
    public void delete(int idSalarie) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO salarie WHERE idSalarie =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idSalarie);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode UPDATE 
    public void update(Salarie sal) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO salarie"
                + "(idSalarie, nomPrenomSalarie)"
                + "VALUES(?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, sal.getIdSalarie());
            ps.setString(2, sal.getNomPrenomSalarie());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
