package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Conference;
import Database.POJO.Conferencier;
import Database.POJO.Salle;
import Database.POJO.Theme;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConferenceDAO {

// creation de la methode getAll //////////////////////////////////////////////////////
    public static ArrayList<Conference> getAll() {

        ArrayList<Conference> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM conference";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idConference = resR.getInt("idConference");
                    String titreConference = resR.getString("titreConference");
                    Conferencier conferencier = ConferencierDAO.get(resR.getInt("idConferencier"));
                    java.sql.Date conference = resR.getDate("dateConference");
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(conference.getTime());
                    Salle salle = SalleDAO.get(resR.getInt("idSalle"));
                    Theme theme = ThemeDAO.get(resR.getInt("idTheme"));
                    Conference conf = new Conference(titreConference, conferencier, cal, salle, theme);
                    listeRetour.add(conf);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println();
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Conference get(int idConference) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT titreConference FROM conference WHERE idConference = ?";
        Conference conf = null;
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idConference);
            resR = ps.executeQuery();

            try {
                while (resR.next()) {
                    String titreConference = resR.getString("titreConference");
                    Conferencier conferencier = ConferencierDAO.get(resR.getInt("idConferencier"));
                    java.sql.Date conference = resR.getDate("dateConference");
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(conference.getTime());
                    Salle salle = SalleDAO.get(resR.getInt("idSalle"));
                    Theme theme = ThemeDAO.get(resR.getInt("idTheme"));
                    conf = new Conference(titreConference, conferencier, cal, salle, theme);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return conf;

    }

    // creation de la methode INSERT pour entree en B2D une conference
    public static void insert(Conference conf) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO conference"
                + "(titreConference, idConferencier, dateConference, idSalle, idTheme)"
                + "VALUES(?,?,?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, conf.getTitreConference());
            ps.setInt(2, conf.getConferencier().getIdConferencier());
            ps.setDate(3, conf.getSqlDate());
            ps.setInt(4, conf.getSalle().getIdSalle());
            ps.setInt(5, conf.getTheme().getIdTheme());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode DELETE 
    public void delete(int idConference) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO conference WHERE idConference =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idConference);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode UPDATE 
    public void update(Conference conf) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO conference"
                + "(idConference,titreConference, idConferencier, dateConference, idSalle, idTheme)"
                + "VALUES(?,?,?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, conf.getIdConference());
            ps.setString(2, conf.getTitreConference());
            ps.setInt(3, conf.getConferencier().getIdConferencier());
            ps.setDate(4, conf.getSqlDate());
            ps.setInt(4, conf.getSalle().getIdSalle());
            ps.setInt(6, conf.getTheme().getIdTheme());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
// Fin de la class
}
