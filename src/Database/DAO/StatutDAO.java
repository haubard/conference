package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Statut;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StatutDAO {

// creation de la methode getAll //////////////////////////////////////////////////////
    public static ArrayList<Statut> getAll() {

        ArrayList<Statut> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM statut";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idStatut = resR.getInt("idStatut");
                    String designationStatut = resR.getString("designationStatut");
                    Statut s = new Statut(idStatut, designationStatut);
                    listeRetour.add(s);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Statut get(int idStatut) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT idStatut, designationStatut FROM statut WHERE idStatut = ?";
        Statut s = null;
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idStatut);
            resR = ps.executeQuery();

            try {
                while (resR.next()) {
                    String designationStatut = resR.getString("designationStatut");
                    s = new Statut(idStatut, designationStatut);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return s;

    }

    // creation de la methode INSERT 
    public static void insert(Statut s) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO Statut"
                + "(designationStatut)"
                + "VALUES(?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, s.getDesignationStatut());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode DELETE 
    public void delete(int idStatut) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO statut WHERE idStatut =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idStatut);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode UPDATE 
    public void update(Statut s) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO statut"
                + "(designationStatut)"
                + "VALUES(?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, s.getDesignationStatut());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
