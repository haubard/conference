package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Inscription;
import Database.POJO.InscriptionStatut;
import Database.POJO.Statut;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InscriptionStatutDAO {

// creation de la methode getAll //////////////////////////////////////////////////////
    public static ArrayList<InscriptionStatut> getAll() {

        ArrayList<InscriptionStatut> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM inscriptionstatut";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    Inscription inscription = InscriptionDAO.get(resR.getInt("idInscription"));
                    Statut statut = StatutDAO.get(resR.getInt("idStatut"));
                    java.sql.Date conference = resR.getDate("dateStatutInscription");
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(conference.getTime());
                    InscriptionStatut is = new InscriptionStatut(inscription, statut, cal);
                    listeRetour.add(is);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static InscriptionStatut get(int idInscriptionStatut) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT idStatut, dateStatutInscription FROM inscriptionstatut WHERE idInscriptionStatut = ?";
        InscriptionStatut is = null;
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idInscriptionStatut);
            resR = ps.executeQuery();

            try {
                while (resR.next()) {
                    Inscription inscription = InscriptionDAO.get(resR.getInt("idInscription"));
                    Statut statut = StatutDAO.get(resR.getInt("idStatut"));
                    java.sql.Date dateStatutInscription = resR.getDate("dateStatutInscription");
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(dateStatutInscription.getTime());
                    is = new InscriptionStatut(inscription, statut, cal);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return is;

    }

    // creation de la methode INSERT 
    public static void insert(InscriptionStatut is) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO inscriptionstatut"
                + "(idStatut, dateStatutInscription)"
                + "VALUES(?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, is.getStatut().getIdStatut());
            ps.setDate(2, is.getSqlDate());

            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode DELETE 
    public void delete(int idInscriptionStatut) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO inscriptionstatut WHERE idInscriptionStatut =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idInscriptionStatut);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode UPDATE 
    public void update(InscriptionStatut is) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO inscriptionstatut"
                + "(idStatut, dateStatutInscription)"
                + "VALUES(?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, is.getStatut().getIdStatut());
            ps.setDate(2, is.getSqlDate());

            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
