package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Conference;
import Database.POJO.Conferencier;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConferencierDAO {

    // creation de la methode getAll //////////////////////////////////////////////////////
    public static ArrayList<Conferencier> getAll() {

        ArrayList<Conferencier> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM conferencier";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idConferencier = resR.getInt("idConferencier");
                    String nomPrenomConferencier = resR.getString("nomPrenomConferencier");
                    String blocNoteConferencier = resR.getString("blocNoteConferencier");
                    boolean conferencierInterne = resR.getBoolean("conferencierInterne");
                    Conferencier conf = new Conferencier(idConferencier, nomPrenomConferencier, conferencierInterne, blocNoteConferencier);
                    listeRetour.add(conf);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println();
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Conferencier get(int idConferencier) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT nomPrenomConferencier, blocNoteConferencier, conferencierInterne FROM conferencier WHERE idConferencier = ?";
        Conferencier conf = null;
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idConferencier);
            resR = ps.executeQuery();

            try {
                while (resR.next()) {
                    String nomPrenomConferencier = resR.getString("nomPrenomConferencier");
                    String blocNoteConferencier = resR.getString("blocNoteConferencier");
                    boolean conferencierInterne = resR.getBoolean("conferencierInterne");
                    conf = new Conferencier(idConferencier, nomPrenomConferencier, conferencierInterne, blocNoteConferencier);
                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return conf;

    }

///////////////creation de la methode INSERT ////////////////////////////
    public static void insert(Conferencier conf) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO conferencier"
                + "(nomPrenomConferencier, conferencierInterne, blocNoteConferencier)"
                + "VALUES(?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, conf.getnomPrenomConferencier());
            ps.setInt(2, conf.getIdConferencierInterne());
            ps.setString(3, conf.blocNoteConferencier());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode DELETE pour supprimer une conference
    public void delete(int idConferencier) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO conferencier WHERE idConferencier =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idConferencier);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode UPDATE pour entree en B2D une conference
    public void update(Conference conf) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO conferencier"
                + "(idConferencier, nomPrenomConferencier, conferencierInterne, idConference, blocNoteConferencier)"
                + "VALUES(?,?,?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, conf.getConferencier().getIdConferencier());
            ps.setString(2, conf.getConferencier().getNomPrenomConferencier());
            ps.setString(3, conf.getConferencier().getNomPrenomConferencier());
            ps.setInt(4, conf.getIdConference());
            ps.setString(5, conf.getConferencier().blocNoteConferencier());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
