package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Conference;
import Database.POJO.Inscription;
import Database.POJO.Salarie;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InscriptionDAO {

// creation de la methode getAll //////////////////////////////////////////////////////
    public static ArrayList<Inscription> getAll() {

        ArrayList<Inscription> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM inscription";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idInscription = resR.getInt("idInscription");
                    Conference conference = ConferenceDAO.get(resR.getInt("idConference"));
                    Salarie salarie = SalarieDAO.get(resR.getInt("idSalarie"));

                    Inscription i = new Inscription(idInscription, salarie, conference);
                    listeRetour.add(i);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Inscription get(int idInscription) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT idConference, idSalarie FROM inscription WHERE idInscription = ?";
        Inscription i = null;
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idInscription);
            resR = ps.executeQuery();

            try {
                while (resR.next()) {
                    Conference conference = ConferenceDAO.get(resR.getInt("idConference"));
                    Salarie salarie = SalarieDAO.get(resR.getInt("idSalarie"));
                    i = new Inscription(idInscription, salarie, conference);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return i;

    }

    // creation de la methode INSERT 
    public static void insert(Inscription ins) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO inscription"
                + "(idConference, idSalarie)"
                + "VALUES(?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, ins.getConference().getIdConference());
            ps.setInt(2, ins.getSalarie().getIdSalarie());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode DELETE 
    public void delete(int idInscription) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO inscription WHERE idInscription =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idInscription);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    // creation de la methode UPDATE 
    public void update(Inscription ins) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO inscription"
                + "(idInscription, idConference, idSalarie)"
                + "VALUES(?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);

            ps.setInt(1, ins.getIdInscription());
            ps.setInt(2, ins.getConference().getIdConference());
            ps.setInt(3, ins.getSalarie().getIdSalarie());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
