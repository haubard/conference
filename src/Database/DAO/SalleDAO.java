package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.Salle;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalleDAO {

    /////////////////// methode getAll ////////////////////////////////////
    public static ArrayList<Salle> getAll() {

        ArrayList<Salle> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM salle";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idSalle = resR.getInt("idSalle");
                    String nomSalle = resR.getString("nomSalle");
                    int nbPlaceSalle = resR.getInt("nbPlaceSalle");
                    Salle sal = new Salle(idSalle, nomSalle, nbPlaceSalle);
                    listeRetour.add(sal);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println();
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static Salle get(int idSalle) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT nomSalle, nbPlaceSalle FROM salle WHERE idSalle = ?";
        Salle sal = null;
        ResultSet resR = null;

        try {

            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idSalle);
            if (ps.execute()) {
                resR = ps.getResultSet();
            }

            try {
                while (resR.next()) {
                    String nomSalle = resR.getString("nomSalle");
                    int nbPlaceSalle = resR.getInt("nbPlaceSalle");
                    sal = new Salle(idSalle, nomSalle, nbPlaceSalle);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return sal;

    }

////////////////////////////////////////////// creation de la methode INSERT ////////////////////////////////
    public static void insert(Salle sal) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO salle"
                + "(nomSalle, nbPlaceSalle)"
                + "VALUES(?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setString(1, sal.getNomSalle());
            ps.setInt(2, sal.getNbPlaceSalle());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

//////////////////////////////////////// creation de la methode DELETE /////////////////////////////////////////////
    public void delete(int idSalle) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO salle WHERE idsalle =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idSalle);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

///////////////////////// creation de la methode UPDATE ////////////////////////////////////////
    public void update(Salle sal) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "UPDATE INTO salle"
                + "(idSalle, nomSalle, nbPlaceSalle)"
                + "VALUES(?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, sal.getIdSalle());
            ps.setString(2, sal.getNomSalle());
            ps.setInt(3, sal.getNbPlaceSalle());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
