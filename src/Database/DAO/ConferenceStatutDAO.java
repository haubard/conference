package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.POJO.ConferenceStatut;
import Database.POJO.Statut;
import com.mysql.jdbc.Connection;
import conferencedujeudi.Main;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConferenceStatutDAO {

    /////////////////// methode getAll ////////////////////////////////////
    public static ArrayList<ConferenceStatut> getAll() {

        ArrayList<ConferenceStatut> listeRetour = new ArrayList<>();

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT * FROM conferencestatut";
        ResultSet resR = null;
        try {
            ps = maconnexionB2D.prepareStatement(query);

            resR = ps.executeQuery();
            try {
                while (resR.next()) {
                    int idConferenceStatut = resR.getInt("idConferenceStatut");
                    Statut statut = StatutDAO.get(resR.getInt("idStatut"));
                    java.sql.Date dateStatutConference = resR.getDate("dateStatutConference");
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(dateStatutConference.getTime());
                    ConferenceStatut confSt = new ConferenceStatut(idConferenceStatut, statut, cal);
                    listeRetour.add(confSt);

                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return listeRetour;
    }

///////////creation d'une methode get(idObjet) ///////////////////////////////////////////////////////
    public static ConferenceStatut get(int idConferenceStatut) {

        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "SELECT idConferenceStatut, idStatut, dateStatutConference FROM conferencestatut WHERE idConferenceStatut = ?";
        ConferenceStatut confSt = null;
        ResultSet resR = null;
        try {

            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idConferenceStatut);
            if (ps.execute()) {
                resR = ps.getResultSet();
            }

            try {
                while (resR.next()) {

                    Statut statut = StatutDAO.get(resR.getInt("idStatut"));
                    java.sql.Date dateStatutConference = resR.getDate("dateStatutConference");
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(dateStatutConference.getTime());

                    confSt = new ConferenceStatut(idConferenceStatut, statut, cal);

                }
            } catch (SQLException ex) {
            }

            //  1  parcourir resultset
            //  2  récupère dans le while tous les attributs cf.Main.java
            //  3  toujours dans la while, je crée un objet à partir des attributs
            //  4  j'ajoute cet objet à une liste que je retourne
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return confSt;

    }

////////////////////////////////////////////// creation de la methode INSERT ////////////////////////////////
    public static void insert(ConferenceStatut confSt) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO conferencestatut"
                + "(idConference, idStatut, dateStatueConference)"
                + "VALUES(?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, confSt.getConference().getIdConference());
            ps.setInt(2, confSt.getStatut().getIdStatut());
            ps.setDate(3, confSt.getSqlDate());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

//////////////////////////////////////// creation de la methode DELETE /////////////////////////////////////////////
    public void delete(int idConferenceStatut) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "DELETE INTO conferencestatut WHERE idConferenceStatut =?";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, idConferenceStatut);
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

///////////////////////// creation de la methode UPDATE ////////////////////////////////////////
    public void update(ConferenceStatut confSt) {
        PreparedStatement ps = null;
        Connection maconnexionB2D = getConnexion();
        String query = "INSERT INTO conferencestatut"
                + "(idConferenceStatut, idConference, idStatut, dateStatueConference)"
                + "VALUES(?,?,?)";

        try {
            ps = maconnexionB2D.prepareStatement(query);
            ps.setInt(1, confSt.getIdConferenceStatut());
            ps.setInt(2, confSt.getConference().getIdConference());
            ps.setInt(3, confSt.getStatut().getIdStatut());
            ps.setDate(4, confSt.getSqlDate());
            ps.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
