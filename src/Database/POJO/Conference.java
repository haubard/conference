package Database.POJO;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;


public class Conference {
/////////////////////////////////////// attribut

    private int idConference;
    private  Conferencier conferencier;
    private String titreConference;
    private Calendar dateConference;
    private int Conference;
    private String nomConferencier;
    private Salle salle;
    private  Theme theme;
    private Date sqlDate;
///////////////////////////////////////: constructeur

    public Conference() {

    }
    
        public Conference(String _titreConference, Conferencier _conferencier, Calendar _dateConference, Salle _salle, Theme _theme) {

     
        this.titreConference = _titreConference;
        this.conferencier = _conferencier;
        this.dateConference = _dateConference;
        this.salle = _salle;
        this.theme = _theme;

        }
        
    public Conference(int _idConference, String _titreConference, Conferencier _conferencier, String _nomConferencier) {

        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.conferencier = _conferencier;
        this.nomConferencier = _nomConferencier;

    }

    public Conference(int _idConference, String _titreConference, Calendar _dateConference, Conferencier _conferencier, String _nomConferencier, Salle _salle, Theme _theme) {

        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.conferencier = _conferencier;
        this.dateConference = _dateConference;
        this.salle = _salle;
        this.theme = _theme;
        this.nomConferencier = _nomConferencier;

    }
    

    //////////////////////////////////////////// methode
    public int getIdConference() {
        return idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }

    public String getTitreConference() {
        return titreConference;
    }

    public void setTitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }

    public Calendar getDateConference() {
        return dateConference;
    }

    public void setDateConference(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }

    public String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateConference().getTimeInMillis());
    }

    public java.sql.Date getSqlDate() {
        java.sql.Date sqlDate = new java.sql.Date(dateConference.getTimeInMillis());
        return sqlDate;
    }

    public Conferencier getConferencier() {
        return conferencier;
    }

    public void Conferencier(Conferencier _conferencier) {
        this.conferencier = conferencier;
    }

    public String getNomConferencier() {
        return nomConferencier;
    }

    public void setNomConferencier(String _nomConferencier) {
        this.nomConferencier = _nomConferencier;
    }

    public Salle getSalle() {
        return salle;
    }

    public void Salle(Salle _salle) {
        this.salle = _salle;
    }

    public Theme getTheme() {
        return theme;
    }

    public void Theme(Theme _Theme) {
        this.theme = _Theme;
    }


  





}
