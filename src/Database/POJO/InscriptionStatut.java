package Database.POJO;

import java.sql.Date;
import java.util.Calendar;

public class InscriptionStatut {

// Attributs
    private Inscription inscription;
    private Statut statut;
    private Calendar dateStatutInscription;

// Constructeurs
    public InscriptionStatut(Inscription _inscription, Statut _statut, Calendar _dateStatutInscription) {

        this.inscription = _inscription;
        this.statut = _statut;
        this.dateStatutInscription = _dateStatutInscription;
    }

// Methodes
    public Inscription getInscription() {
        return inscription;
    }

    public void  setInscription(Inscription inscription) {
        this.inscription = inscription;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Calendar getDateStatutInscription() {
        return dateStatutInscription;
    }

    public void setDateStatutInscription(Calendar dateStatutInscription) {
        this.dateStatutInscription = dateStatutInscription;
    }
  

    public java.sql.Date getSqlDate() {
        java.sql.Date sqlDate = new java.sql.Date(dateStatutInscription.getTimeInMillis());
        return sqlDate;
}
    // Fin de la classe 
}