
package Database.POJO;

public class Salarie {
    
// Attributs    
    
    private int idSalarie;
    private String nomPrenomSalarie;
    
    
// Constructeurs

public Salarie(int _idSalarie, String _nomPrenomSalarie) {
    
    this.idSalarie = _idSalarie;
    this.nomPrenomSalarie = _nomPrenomSalarie;
} 

// Methodes
    public int getIdSalarie() {
        return idSalarie;
    }

    public void setIdSalarie(int idSalarie) {
        this.idSalarie = idSalarie;
    }

    public String getNomPrenomSalarie() {
        return nomPrenomSalarie;
    }

    public void setNomPrenomSalarie(String nomPrenomSalarie) {
        this.nomPrenomSalarie = nomPrenomSalarie;
    }


 // Fin de la classe   
}
