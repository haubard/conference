package Database.POJO;

public class Statut {

// Attributs
    
    
    private int idStatut;
    private String designationStatut;

// Constructeurs
    
    
    public Statut(int _idStatut, String _designationStatut) {

        this.idStatut = _idStatut;
        this.designationStatut = _designationStatut;
    }

// Methodes
    
    
    public int getIdStatut() {
        return idStatut;
    }

    public void setIdStatut(int idStatut) {
        this.idStatut = idStatut;
    }

    public String getDesignationStatut() {
        return designationStatut;
    }

    public void setDesignationStatut(String designationStatut) {
        this.designationStatut = designationStatut;
    }

    // Fin de la classe   
}
