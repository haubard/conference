
package Database.POJO;

public class Salle {
    
// Attibuts
    
    private int idSalle;
    private String nomSalle;
    private int nbPlaceSalle;
    

// Constructeurs

public Salle(int _idSalle, String _nomSalle, int _nbPlaceSalle){
    
    this.idSalle = _idSalle;
    this.nomSalle = _nomSalle;
    this.nbPlaceSalle = _nbPlaceSalle;
    
} 

// Methodes

    public int getIdSalle() {
        return idSalle;
    }

    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }

    public String getNomSalle() {
        return nomSalle;
    }

    public void setNomSalle(String nomSalle) {
        this.nomSalle = nomSalle;
    }

    public int getNbPlaceSalle() {
        return nbPlaceSalle;
    }

    public void setNbPlaceSalle(int nbPlaceSalle) {
        this.nbPlaceSalle = nbPlaceSalle;
    }
    
    @Override
    
    public String toString(){
        return this.nomSalle;
    }
           
    
    
// Fin de la classe    
}
