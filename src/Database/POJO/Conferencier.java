package Database.POJO;

public class Conferencier {

    // Attributs
    private int idConferencier;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private String blocNoteConferencier;

// Constructeurs
    public Conferencier() {

    }

    public Conferencier(int _idConferencier, String _nomPrenomConferencier) {

        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenomConferencier;
    }

    public Conferencier(int _idConferencier, String _nomPrenomConferencier, boolean _conferencierInterne,  String _blocNoteConferencier) {

        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenomConferencier;
        this.conferencierInterne = _conferencierInterne;
        this.blocNoteConferencier = _blocNoteConferencier;
    }

// Methodes
    public int getIdConferencier() {
        return idConferencier;
    }

    public void setIdConferencier(int idConferencier) {
        this.idConferencier = idConferencier;
    }

    public String getNomPrenomConferencier() {
        return nomPrenomConferencier;
    }

    public void setNomPrenomConferencier(String nomPrenomConferencier) {
        this.nomPrenomConferencier = nomPrenomConferencier;
    }

    public Boolean getConferencierInterne() {
        return conferencierInterne;
    }

    public void setConferencierInterne(Boolean conferencierInterne) {
        this.conferencierInterne = conferencierInterne;
    }


    public String getBlocNoteConferencier() {
        return blocNoteConferencier;
    }

    public void setBlocNoteConferencier(String blocNoteConferencier) {
        this.blocNoteConferencier = blocNoteConferencier;
    }
    
    
    //////// Fin de la classe ////////////////////////////////   

    public String getnomPrenomConferencier() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdConferencierInterne() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String blocNoteConferencier() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
       @Override
    
    public String toString(){
        return this.nomPrenomConferencier;
    }
}
