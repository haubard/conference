
package Database.POJO;

public class Theme {
    
// Attributs
    
    private int idTheme;
    private String designationTheme;
    
// Constructeurs

public Theme(int _idTheme, String _designationTheme) {
    
    this.idTheme = _idTheme;
    this.designationTheme = _designationTheme;
    
}   

// Methodes


    public int getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }

    public String getDesignationTheme() {
        return designationTheme;
    }

    public void setDesignationTheme(String designationTheme) {
        this.designationTheme = designationTheme;
    }
    
       @Override
    
    public String toString(){
        return this.designationTheme;
    }


// Fin de la classe    
}
