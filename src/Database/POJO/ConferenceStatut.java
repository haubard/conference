package Database.POJO;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ConferenceStatut {

// Attributs
    
    private int idConferenceStatut;
    private Conference conference;
    private Statut statut;
    private Calendar dateStatutConference;
    private Date SqlDate;

// Constructeurs
    
    
    public ConferenceStatut(int _idConferenceStatut, Conference conference, Statut statut, Calendar _dateStatutConference) {
        this.idConferenceStatut = _idConferenceStatut;
        this.conference = conference;
        this.statut = statut;
        this.dateStatutConference = _dateStatutConference;

    }



    public ConferenceStatut(int _idConferenceStatut,Statut statut, Calendar cal) {
        
    }
    // Methodes
    
    public int getIdConferenceStatut(){
        return idConferenceStatut;
    }
    
    public Conference getConference() {
        return conference;
    }

    public void setConference(Conference conference) {
        this.conference = conference;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Calendar getDateStatutConference() {
        return dateStatutConference;
    }

    public void setDateStatutConference(Calendar dateStatutConference) {
        this.dateStatutConference = dateStatutConference;
    }
        public String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateStatutConference().getTimeInMillis());
    }
            public java.sql.Date getSqlDate() {
        java.sql.Date sqlDate = new java.sql.Date(dateStatutConference.getTimeInMillis());
        return sqlDate;

    // Fin de la classe  
}
}
