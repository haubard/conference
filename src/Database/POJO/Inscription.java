package Database.POJO;

public class Inscription {

    // Attributs
    
    
    private int idInscription;
    private Salarie salarie;
    private Conference conference;

    // Constructeurs
    
    
    public Inscription(int _idInscription, Salarie _salarie, Conference _conference) {

        this.idInscription = _idInscription;
        this.salarie = _salarie;
        this.conference = _conference;

    }

    // Methodes

    public int getIdInscription() {
        return idInscription;
    }

    public void setIdInscription(int idInscription) {
        this.idInscription = idInscription;
    }

    public Salarie getSalarie() {
        return salarie;
    }

    public void setSalarie(Salarie salarie) {
        this.salarie = salarie;
    }

    public Conference getConference() {
        return conference;
    }

    public void setConference(Conference conference) {
        this.conference = conference;
    }
    
    
    // Fin de la classe 

    
}
