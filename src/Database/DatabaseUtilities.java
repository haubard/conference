package Database;

import static Database.DatabaseUtilities.exec;
import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseUtilities {

    // methode qui renvoie un objet de type connection
    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        Connection connection = null;

        try {
            connection = (Connection) DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/conference_du_jeudi", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    // methode qui renvoie un objet de type String ResultSet (EXECUTER REQUETE SIMPLE)...
    public static ResultSet exec(String query) {
        Connection maconnexionB2D = getConnexion();
        Statement stmt = null;
        ResultSet resultatR = null;

        try {
            stmt = maconnexionB2D.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            resultatR = stmt.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultatR;
    }

    // fin de la classe DatabaseUtilities   
}
