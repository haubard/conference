package Model;

import Database.POJO.Conference;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class ConferenceModel extends AbstractTableModel {

    ////////////////////////////////////://attribut
    private ArrayList<Conference> listeConference = new ArrayList();
    private String[] columnsName = new String[]{"Titre", "Date", "Conférencier", "Thème", "Salle"};

   //////////////////////////////////////contructeur
    public ConferenceModel(ArrayList<Conference> _conferences) {
        this.listeConference = _conferences;

    }

    /////////////////////////////////////////méthodes
    @Override
    public int getRowCount() {
        return listeConference.size();
    }

    public String getColumnName(int columnIndex) {
        return columnsName[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Conference maConf = listeConference.get(rowIndex);

        switch (columnIndex) {

            case 0: {
                return maConf.getTitreConference();
            }
            case 1: {
                return maConf.getDateString();
            }
            case 2: {
                return maConf.getConferencier().getNomPrenomConferencier();
            }
            case 3: {
                return maConf.getTheme().getDesignationTheme();
            }
            case 4: {
                return maConf.getSalle().getNomSalle();
            }
            default:
                return "";
        }

    }

}
