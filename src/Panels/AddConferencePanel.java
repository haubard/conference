package Panels;

import Database.DAO.ConferenceDAO;
import Database.DAO.ConferencierDAO;
import Database.DAO.SalleDAO;
import Database.DAO.ThemeDAO;
import Database.POJO.Conference;
import Database.POJO.Conferencier;
import Database.POJO.Salle;
import Database.POJO.Theme;
import conferencedujeudi.CustomWindow;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class AddConferencePanel extends JPanel {
    JPanel action;

    public AddConferencePanel() {
        action = this;
////////////////////////////// Zone de saisie
        JTextField titre = new JTextField();
        JLabel labTitre = new JLabel("Titre de la conférence");
        titre.setColumns(10);
        this.add(titre);

        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        JLabel labDate = new JLabel("Date de la conférence");
        JFormattedTextField dateTextField = new JFormattedTextField(simple);
        dateTextField.setColumns(10);
        this.add(dateTextField);
///////////////////// Combo Box  choisir la salle
        JComboBox salle = new JComboBox(SalleDAO.getAll().toArray());
        this.add(salle);
////////////////////  Combo Box Choisir le thème        
        JComboBox theme = new JComboBox(ThemeDAO.getAll().toArray());
        this.add(theme);
////////////////////  Combo Box Choisir le conferencier        
        JComboBox conferencier = new JComboBox(ConferencierDAO.getAll().toArray());
        this.add(conferencier);

//////////////// Creation du bouton valider
        JButton btnValider = new JButton("Valider");
///////////////// Creation du label du bouton    
        JLabel labelBoutVal = new JLabel("");
///////////////// Couleur du label du bouton
        labelBoutVal.setForeground(Color.WHITE);
///////////////// Couleur de fond du bouton    
        btnValider.setBackground(Color.lightGray);
////////////////// Dimension du bouton    
        btnValider.setBounds(250, 300, 300, 150);
//////////////// Creation du bouton supprimer
        JButton btnSup = new JButton("Supprimer");
///////////////// Creation du label du bouton    
        JLabel labelBoutSup = new JLabel("");
///////////////// Couleur du label du bouton
        labelBoutSup.setForeground(Color.WHITE);
///////////////// Couleur de fond du bouton    
        btnSup.setBackground(Color.lightGray);
////////////////// Dimension du bouton    
        btnSup.setBounds(300, 350, 350, 200);
        JButton btnModif = new JButton("Modifier");
///////////////// Creation du label du bouton    
        JLabel labelBoutModif = new JLabel("");
///////////////// Couleur du label du bouton
        labelBoutModif.setForeground(Color.WHITE);
///////////////// Couleur de fond du bouton    
        btnModif.setBackground(Color.lightGray);
////////////////// Dimension du bouton    
        btnModif.setBounds(300, 350, 350, 200);

        // Organisatioon du panel en box 
        Box b4 = Box.createHorizontalBox();
        b4.add(btnValider);
        b4.add(btnModif);
        b4.add(btnSup);

        Box b3 = Box.createHorizontalBox();
        b3.add(labDate);
        b3.add(dateTextField);

        Box b2 = Box.createHorizontalBox();
        b2.add(labTitre);
        b2.add(titre);

        Box b1 = Box.createVerticalBox();
        b1.add(b2);
        b1.add(b3);
        b1.add(conferencier);
        b1.add(theme);
        b1.add(salle);
        b1.add(b4);
        
         
        Box superBox = Box.createVerticalBox();
        superBox.add(Box.createRigidArea(new Dimension(500, 250)));
        superBox.add(b2);
        superBox.add(b3);
        superBox.add(b1);
        superBox.add(b4);
        this.add(superBox);

        btnValider.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {
                try {
                    String dateString = dateTextField.getText();
                    java.util.Date date = date = simple.parse(dateString);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    Conference insertConf = new Conference(titre.getText(), (Conferencier) conferencier.getSelectedItem(), cal, (Salle) salle.getSelectedItem(), (Theme) theme.getSelectedItem());
                    ConferenceDAO.insert(insertConf);
                } catch (ParseException ex) {
                    Logger.getLogger(AddConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(null, "Votre saisie en Base de Données s'est bien déroulée !!!");
                
                CustomWindow cw = (CustomWindow) action.getTopLevelAncestor();
                VoirListeConference voirList = new VoirListeConference();
                cw.setContentPane(voirList);
                cw.repaint();
                cw.revalidate();

            }

        });
///////////////////// Fin du panel formulaire/////////////////////////
    }
////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN DE LA CLASS///////////////////////////   



}
