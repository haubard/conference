package Panels;

import Database.DAO.ConferenceDAO;
import Model.ConferenceModel;
import Database.POJO.Conference;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class Home extends JPanel {

    private int nbClic = 0;
///////////////////// constructeur

    public Home() {

        Calendar c = Calendar.getInstance();
        JLabel label = new JLabel("Bienvenue sur l'application");
        // couleur du label
        label.setForeground(Color.WHITE);
        JButton bouton1 = new JButton("CLICK ......... et surprise");
        JLabel labelBout = new JLabel("");
        // couleur du labelBouton
        labelBout.setForeground(Color.WHITE);
//        // ajouter le label
//        this.add(label);
//        // ajouter le bouton
//        this.add(bouton1);
//        // ajouter le label du bouton
//        this.add(labelBout);
        // dimension du bouton
        bouton1.setBounds(250, 300, 300, 150);
        // couleur du bouton
        bouton1.setBackground(Color.lightGray);
        // Action en attente (sur evenement) sur le bouton
        bouton1.addActionListener(new ActionListener() {

///////////////////// methodes 
            // compteur incrementé sur clic du bouton1
            @Override
            public void actionPerformed(ActionEvent e) {
                nbClic = nbClic + 1;
                labelBout.setText("Vous avez cliquez " + nbClic + " fois sur le bouton");
            }
        });
        // creation d'une nouvelle liste pour mes conf
        ArrayList<Conference> lesConf = ConferenceDAO.getAll();
        // creer 4 conferences en dur dans le code
//        Conference conf1 = new Conference(1, "Premiere conference", c, 1, "Hervé AUBARD", 1, 1);
//        Conference conf2 = new Conference(2, "Deuxieme conference", c, 1, "Jeff BEZOS ", 2, 2);
//        Conference conf3 = new Conference(3, "Troisieme conference", c, 1, "Xavier NIEL", 3, 3);
//        Conference conf4 = new Conference(4, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
//        Conference conf5 = new Conference(5, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
//        Conference conf6 = new Conference(6, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
//        Conference conf7 = new Conference(7, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
//        Conference conf8 = new Conference(8, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
//        Conference conf9 = new Conference(9, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
//        Conference conf10 = new Conference(10, "Quatrieme conference", c, 1, "Mark ZUKERBERG", 4, 4);
        // ajouter mes 3 conferences à ma list de conf
//        lesConf.add(conf1);
//        lesConf.add(conf2);
//        lesConf.add(conf3);
//        lesConf.add(conf4);
//        lesConf.add(conf5);
//        lesConf.add(conf6);
//        lesConf.add(conf7);
//        lesConf.add(conf8);
//        lesConf.add(conf9);
//        lesConf.add(conf10);
        // creer un objet COnferenceModele ayant pour param ma list de conf
        ConferenceModel confMod = new ConferenceModel(lesConf);
        // creer une nouvelle table
        JTable table1 = new JTable(confMod);
        // donner une dimension a la table
      table1.setSize(1000, 1000);
        // rendre la table triable
        table1.setAutoCreateRowSorter(true);
        // centrer les données dans le tableau
        DefaultTableCellRenderer cr = new  DefaultTableCellRenderer();
        cr.setHorizontalAlignment(SwingConstants.CENTER);
//        table1.getColumn("Titre Conference").setCellRenderer(cr);
//        table1.getColumn("Identifiant conferencier").setCellRenderer(cr);
//        table1.getColumn("Date de la conference").setCellRenderer(cr);
//        table1.getColumn("Salle").setCellRenderer(cr);
//        table1.getColumn("Thème").setCellRenderer(cr);
       // Permet de faire ascenceur sur le defilement
        JScrollPane scroll = new JScrollPane(table1);

        // Organisatioon du panel en box vertical
        Box b2 = Box.createVerticalBox();
        b2.add(label);

        Box b3 = Box.createHorizontalBox();
        b3.add(bouton1);
        b3.add(labelBout); 

        Box b4 = Box.createVerticalBox();
        b4.add(scroll);

        Box b1 = Box.createVerticalBox();
        b1.add(Box.createRigidArea(new Dimension(950, 100)));
        this.add(b1);
        b1.add(b2);
        b1.add(b3);
        b1.add(b4);

        // couleur de fond de l'objet Home
        this.setBackground(Color.DARK_GRAY);

    }

}
